#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/kvm.h>

#define KVM_DEV     "/dev/kvm"

int main(void)
{
    int kvm;

    if ((kvm = open(KVM_DEV, O_RDWR)) < 0) {
        perror("Opening " KVM_DEV);
        return 2;
    }

    int apiver = ioctl(kvm, KVM_GET_API_VERSION, 0);
    if (apiver < 0) {
        perror("ioctl(KVM_GET_API_VERSION)");
        return 2;
    }

    printf("KVM API Version: %d\n", apiver);

    close(kvm);
    return 0;
}
